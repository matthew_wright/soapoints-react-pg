const dd = require("ES/lib/dd");

const RegisterView = dd.createClass({
    _formOnSubmitHandler: function _formOnSubmitHandler(event) {
        event.preventDefault();

        //user.setAll({
        //    firstName: event.target.firstName.value,
        //    lastName: event.target.lastName.value,
        //    email: event.target.email.value,
        //    username: event.target.username.value,
        //    password: event.target.password.value
        //});
    },
    _cancelTouchHandler: function _cancelTouchHandler(event) {
        event.preventDefault();

        if(this.props.changeViewHandler) {
            this.props.changeViewHandler("LoginView");
        }
    },
    render: function render() {
        var errorMessage;

        // TODO: account for warnings too?
        // if (this.state.error) {
        // errorMessage = <ErrorMessageContainer message={this.state.error}/>;
        // }

        return <div>
            <form onSubmit={this._formOnSubmitHandler}>
                {errorMessage}
                <div className="form-group">
                    <label htmlFor="firstNameInput">First Name:</label>
                    <input type="text" placeholder="First Name"
                           id="firstNameInput" name="firstName"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="lastNameInput">Last Name:</label>
                    <input type="text" placeholder="Last Name"
                           id="lastNameInput" name="lastName"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="emailInput">Email:</label>
                    <input type="text" placeholder="me@example.com"
                           id="emailInput" name="email"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="usernameInput">Username:</label>
                    <input type="text" placeholder="Username"
                           id="usernameInput" name="username"
                           className="form-control"/>
                </div>
                <div className="form-group">
                    <label htmlFor="passwordInput">Password:</label>
                    <input type="password" placeholder="************"
                           id="passwordInput" name="password"
                           className="form-control"/>
                </div>
                <button type="submit" className="btn btn-primary">Register</button>
                <button type="button"
                        className="btn btn-default"
                        onTouch={this._cancelTouchHandler}>Cancel
                </button>
            </form>
        </div>;
    }
});

module.exports = RegisterView;