'use strict';

import React from 'react';

const ErrorMessageContainer = React.createClass({
    render: function render() {
        console.log("ErrorMessageContainer");
        console.log(this.props);

        return <div className="danger">
            {this.props.message.error}
        </div>;
    }
});

export default ErrorMessageContainer;