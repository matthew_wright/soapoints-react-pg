'use strict';

import React from 'react';
import MultiMessageContainer from './messages/MultiMessageContainer';

const wrapperFactory = function () {

    const reactObj = React.createClass({
        render: function render() {
            // can put navigation elements here
            return <div className="container">
                <MultiMessageContainer/>
                {this.props.children}
            </div>
        }
    });

    return reactObj;
};

const WrapperView = wrapperFactory();

export default WrapperView;
