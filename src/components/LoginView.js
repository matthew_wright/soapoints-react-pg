const dd = require("ES/lib/dd");
const FormMixin = require("ES/lib/react-loose-forms");
const FormButtons = require("ES/lib/react-loose-forms.bootstrap3/layouts/FormButtons");
const VerticalFields = require("ES/lib/react-loose-forms.bootstrap3/layouts/VerticalFields");

const LoginView = dd.createClass({
    mixins: [FormMixin],
    buildSchema(){
        return {
            username: {
                type: "text",
                placeholder: "Username"
            },
            password: {
                type: "password",
                placeholder: "********"
            }
        };
    },
    onFormSubmit(user) {
        console.log(user);
    },
    _registerTouchHandler(event) {
        event.preventDefault();

        if(this.props.changeViewHandler) {
            this.props.changeViewHandler("RegisterView");
        }
    },
    render() {
        console.log("LoginView Render");
        console.log(this.props);

        return dd.div(null,
                dd.form({onSubmit: this.Form_onSubmit},
                    VerticalFields({
                        fields: this.Form_buildSchema(),
                        errors: this.state.errors || {},
                        buildInput: this.Form_buildInput
                    }),
                    FormButtons({submit_btn_text: "Log in"})
                ),

                dd.div(null,
                    "Need an account?",
                    dd.a({href: "#", onTouch: this._registerTouchHandler}, "Click Here"),
                    " to register!"
                )
            );
    }
});

module.exports = LoginView;
