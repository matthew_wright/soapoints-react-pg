'use strict';

import {createStore, combineReducers, applyMiddleware, compose} from 'redux';
// Redux DevTools store enhancers
import { devTools, persistState } from 'redux-devtools';

import InitialState from './InitialState';
import AuthReducer from '../reducers/AuthReducer';
import TasksReducer from '../reducers/TasksReducer';
import MessagesReducer from '../reducers/MessagesReducer';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';

const loggerMiddleware = createLogger();

const appReducer = combineReducers({
    auth: AuthReducer,
    tasks: TasksReducer,
    messages: MessagesReducer
});

const appStore = compose(
    applyMiddleware(thunkMiddleware, loggerMiddleware)
)(createStore)(appReducer, InitialState);

export default appStore;
