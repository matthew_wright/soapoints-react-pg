'use strict';

let React = require("ES/lib/react");
import {Provider} from 'react-redux';
import {Router} from 'react-router';
import appStore from './AppStore';
import routes from './Routes';
import History from './History';

const SoaPointsApp = React.createClass({
    render: function render() {
        return <div>
            <Provider store={appStore}>
                <Router history={History} routes={routes}/>
            </Provider>
        </div>;
    }
});

export default SoaPointsApp;
