'use strict';

import {MESSAGE_ACTIONS} from '../enums/Actions';

export function addSystemMessage(message) {
    return {
        type: MESSAGE_ACTIONS.ADD,
        message
    };
}

export function clearSystemMessages() {
    return {
        type: MESSAGE_ACTIONS.CLEAR
    };
}