'use strict';

import {AUTH_ACTIONS} from '../enums/Actions';
import userServiceFactory from '../services/UserService';
import {addSystemMessage, clearSystemMessages} from './MessageActions';
import History from '../core/History';

// Can trigger loading spinner
function submitLogin(user) {
    return {
        type: AUTH_ACTIONS.SUBMIT_LOGIN,
        user
    };
}

function loginSuccess(user) {
    return {
        type: AUTH_ACTIONS.LOGIN_SUCCESS,
        user
    };
}

export function login(user) {
    const UserService = userServiceFactory({});

    return (dispatch) => {
        // clear any error messages we might have
        dispatch(clearSystemMessages());
        dispatch(submitLogin(user));

        return UserService.login(user, (responseData) => {
                dispatch(loginSuccess(responseData));

                // TODO: better place for this?
                // TODO: this seems off.  If you do #/tasks it doesn't work
                // but just doing /tasks doesn't put the # in the URL like Link does...
                History.replaceState(null, '/tasks');
            },
            (errorObj) => {
                dispatch(addSystemMessage(errorObj));
            });
    };
}