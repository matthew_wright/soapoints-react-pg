const ReactDOM = require("ES/lib/react-dom");
const dd = require("ES/lib/dd");
const LoginView = require('./components/LoginView');
//const RegisterView = require('./components/RegisterView');

require("ES/lib/react-loose-forms.bootstrap3").install(require("ES/lib/react-loose-forms/InputTypes"));

const SoaPointsApp = dd.createClass({
    getInitialState() {
        return {
            currentView: "LoginView"
        };
    },
    _determineView() {
        switch(this.state.currentView) {
            case "RegisterView":
                //return RegisterView({
                //    changeViewHandler: this._changeViewHandler
                //});
            case "LoginView":
            default:
                return LoginView({
                    changeViewHandler: this._changeViewHandler
                });
        }
    },
    _changeViewHandler(newView) {
        this.setState({
            currentView: newView
        });
    },
    render() {
        return dd.div({className: "container"},
            this._determineView()
        );
    }
});

ReactDOM.render(SoaPointsApp(), document.getElementById('soapoints-react'));
