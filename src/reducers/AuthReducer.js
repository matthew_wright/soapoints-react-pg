'use strict';

import {AuthInitialState} from '../core/InitialState';
import {AUTH_ACTIONS} from '../enums/Actions';
import assign from 'object-assign';

export default (state, action) => {
    let newState = assign({}, state);

    switch (action.type) {
        case AUTH_ACTIONS.LOGIN_SUCCESS:
            newState = assign(newState, {
                authenticatedUser: action.user
            });
            return newState;
        case AUTH_ACTIONS.LOGOUT:
            newState = assign(newState, {
                authenticatedUser: undefined
            });
            return newState;
        default:
            return state || AuthInitialState;
    }
};