'use strict';

import {MessagesInitialState} from '../core/InitialState';
import {MESSAGE_ACTIONS} from '../enums/Actions';
import assign from 'object-assign';

export default (state, action) => {
    let newState = assign({}, state);

    switch (action.type) {
        case MESSAGE_ACTIONS.ADD:
            newState.messages.push(action.message);
            return newState;
        case MESSAGE_ACTIONS.CLEAR:
            newState.messages = [];
            return newState;
        default:
            return state || MessagesInitialState;
    }

};